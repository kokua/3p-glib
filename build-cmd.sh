#!/bin/bash/env

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"

PROJECT=glib
LICENSE=README
VERSION="2.55.2"
SOURCE_DIR="$PROJECT"

pwd
# Save for later
#GLIB_SOURCE_DIR="$PROJECT"
#VERSION="$(sed -n 's/^ *VERSION=\([0-9.]*\)$/\1/p' "../$GLIB_SOURCE_DIR/configure")"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi


stage="$(pwd)"
PCRE_INCLUDE="${stage}"/packages/include/pcre

[ -f "$PCRE_INCLUDE"/pcre.h ] || fail "You haven't installed the pcre package yet."

# load autobuild provided shell functions and variables
source_environment_tempfile="$stage/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

build=${AUTOBUILD_BUILD_ID:=0}
echo "${VERSION}.${build}" > "${stage}/VERSION.txt"
case "$AUTOBUILD_PLATFORM" in
    "linux")
        # Prefer gcc-4.6 if available.
        if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            export CC=/usr/bin/gcc-4.6
            export CXX=/usr/bin/g++-4.6
        fi
        # Default target to 32-bit
        opts="${TARGET_OPTS:--m32}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        pushd "$TOP/$SOURCE_DIR"

            # libtool was ingoring the LDFLAGS option so the only way to force
            # both the compile and link steps to use -m32 was to redefine CC
            # as below.  Sorry for the hack.
            CFLAGS="-I$PCRE_INCLUDE $opts -O3 -fPIC -DPIC" CXXFLAGS="-I$PCRE_INCLUDE $opts -O3 -fPIC -DPIC" \
            LDFLAGS="-L$stage/packages/lib/release" \
 ./configure --with-pcre=system --prefix="$stage" 
            make
            make install
		mkdir -p "$stage/include/glib"
	    cp -a glib/*.h "$stage/include/glib/"
	    cp -a glib/glibconfig.h "$stage/include/glib-2.0/"
		mkdir -p "$stage/include/glib/deprecated"
	    cp -a glib/deprecated/*.h "$stage/include/glib/deprecated/"
		mkdir -p "$stage/include/gio"
	    cp -a gio/*.h "$stage/include/gio/"
		mkdir -p "$stage/include/gmodule"
	    cp -a gmodule/*.h "$stage/include/gmodule/"
		mkdir -p "$stage/include/gobject"
	    cp -a gobject/*.h "$stage/include/gobject/"
        popd
 
        mv lib release
        mkdir -p lib
        mv release lib

    ;;
    "linux64")
        

        # Default target to 64-bit
        opts="${TARGET_OPTS:--m64}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        # Debug first
#        pushd "$TOP/$SOURCE_DIR"
#		    CFLAGS="$opts -O0 -g -fPIC -DPIC" CXXFLAGS="$opts -O0 -g -fPIC -DPIC" \
#		    ./configure --prefix="$stage" --enable-debug --includedir="$stage/include/glib" 
#		    make
#		    make install

        # conditionally run unit tests
#        if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
#            make test
#        fi
#        # clean the build artifacts
#        make distclean
#       popd 
       # Release last
        pushd "$TOP/$SOURCE_DIR"

            CFLAGS="-I$PCRE_INCLUDE $opts -O3 -fPIC -DPIC -D_GLIBCXX_USE_CXX11_ABI=0" CXXFLAGS="-I$PCRE_INCLUDE $opts -O3 -fPIC -DPIC -D_GLIBCXX_USE_CXX11_ABI=0" \
            LDFLAGS="-L$stage/packages/lib/release" \
            ./configure --with-pcre=system --prefix="$stage" 
            make
            make install
			mkdir -p "$stage/include/glib"
		    cp -a glib/*.h "$stage/include/glib/"
	    	    cp -a glib/glibconfig.h "$stage/include/glib-2.0/"
			mkdir -p "$stage/include/glib/deprecated"
		    cp -a glib/deprecated/*.h "$stage/include/glib/deprecated/"
			mkdir -p "$stage/include/gio"
		    cp -a gio/*.h "$stage/include/gio/"
			mkdir -p "$stage/include/gmodule"
		    cp -a gmodule/*.h "$stage/include/gmodule/"
			mkdir -p "$stage/include/gobject"
		    cp -a gobject/*.h "$stage/include/gobject/"
        popd

        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        echo "platform not supported"
        exit -1
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$SOURCE_DIR/$LICENSE" "$stage/LICENSES/$PROJECT.txt"






